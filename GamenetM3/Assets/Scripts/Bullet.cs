using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
public class Bullet : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        //if (collision.gameObject.CompareTag("Player") && !collision.gameObject.GetComponent<PhotonView>().IsMine)
        //{
        //    collision.gameObject.GetComponent<Shooting>().TakeDamageThruBullet();
        //}
        Destroy(gameObject);
    }

    private void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * 200);
    }
}
