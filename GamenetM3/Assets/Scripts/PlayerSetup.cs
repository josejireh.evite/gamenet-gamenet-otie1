using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using TMPro;
public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera playerCamera;
    public TextMeshProUGUI Name;
    public bool isCheckingInput = true;

    private Shooting shooting;
    // Start is called before the first frame update
    void Start()
    {
        shooting = GetComponent<Shooting>();

        Name.text = photonView.Owner.NickName;
        this.playerCamera = transform.Find("Camera").GetComponent<Camera>();
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;
            GetComponent<Shooting>().enabled = false;
            transform.Find("Gun").gameObject.SetActive(false);
            playerCamera.enabled = photonView.IsMine;
            isCheckingInput = false;
        }
        else if(PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = false;
            //GetComponent<Shooting>().enabled = photonView.IsMine;
            playerCamera.enabled = photonView.IsMine;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(photonView.IsMine)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && isCheckingInput)
            {
                object playerSelectionNumber;
                if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
                {
                    if((int)playerSelectionNumber == 0)
                    {
                        shooting.FireBullets();
                    }
                    else if((int)playerSelectionNumber == 1)
                    {
                        shooting.FireLaser();
                    }
                    else if ((int)playerSelectionNumber == 2)
                    {
                        shooting.FireBullets();
                    }
                }
            }
        }
    }
}
