using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using Photon.Pun;
using TMPro;
using Photon.Realtime;
using ExitGames.Client.Photon;

public enum RaiseEventsCode
{
    WhoDiedCode = 0
}

public class Shooting : MonoBehaviourPunCallbacks
{


    [SerializeField] LayerMask layermask;

    public GameObject HitEffectPrefab;
    public GameObject Gun;
    public GameObject GunBarrel;
    public GameObject Bullet;

    [Header("HP Related Stuff")]
    public float startHealth;
    private float health = 100;
    public Image healthBar;

    private Animator animator;

    public GameObject WhoKilledWhoPrefabParent;
    public GameObject WhoKilledWhoPrefab;

    int playerDeath = 0;
    bool isDead = false;
    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }
    void OnEvent(EventData photonEvent)
    {
        // Filter it because it recieves all kinds of events that will make the callback execute randomly when it recieves a random event
        if (photonEvent.Code == (byte)RaiseEventsCode.WhoDiedCode)
        {
            object[] data = (object[])photonEvent.CustomData;
            playerDeath += 1;


            GameObject[] go = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject g in go)
            {
                if (g.GetComponent<PhotonView>().Owner.NickName == (string)data[0])
                {
                    g.GetComponent<Shooting>().isDead = true;
                    g.GetComponent<PlayerSetup>().isCheckingInput = false;
                    g.GetComponent<VehicleMovement>().enabled = false;
                    g.GetComponent<BoxCollider>().enabled = false;
                }
            }

            if (playerDeath >= DeathRaceManager.instance.MaxPlayer)
            {
                if (!isDead)
                {

                    GameObject UIForWinner = Instantiate(WhoKilledWhoPrefab, WhoKilledWhoPrefabParent.transform);
                    UIForWinner.gameObject.transform.SetParent(WhoKilledWhoPrefabParent.transform);
                    UIForWinner.transform.Find("PlayerNameText").GetComponent<Text>().text = photonView.Owner.NickName + " won the game!";
                }



                //Invoke("LeaveRoom", 5.0f);
            }
        }
    }

    private void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        animator = GetComponent<Animator>();
        WhoKilledWhoPrefabParent = GameObject.Find("PlayerList");
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            Debug.Log(DeathRaceManager.instance.MaxPlayer);
            Debug.Log(playerDeath);
        }

    }

    public void FireLaser()
    {
        RaycastHit hit;
        Ray ray = new Ray(Gun.transform.position, Gun.transform.up);

        if (Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);

            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
            }
        }
    }

    //public void TakeDamageThruBullet()
    //{
    //    gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 50);
    //}
    public void FireBullets()
    {
        PhotonNetwork.Instantiate(Bullet.name, GunBarrel.transform.position, GunBarrel.transform.rotation);
        RaycastHit hit;
        Ray ray = new Ray(Gun.transform.position, Gun.transform.up);

        if (Physics.Raycast(ray, out hit, 200, layermask))
        {
            //Debug.Log(hit.collider.gameObject.name);

            //photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);
            if(hit.collider != null)
            {
                if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
                {
                    hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 50);
                }
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        health -= damage;
        healthBar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            Die(info);
            Debug.Log(photonView.name + " Died");
            Debug.Log(info.Sender.NickName + " Killed " + info.photonView.Owner.NickName);

            // Instantiing a prefab using the layout group thingy that can stack even if there is a lot if kills
            GameObject UI = Instantiate(WhoKilledWhoPrefab, WhoKilledWhoPrefabParent.transform);
            UI.gameObject.transform.SetParent(WhoKilledWhoPrefabParent.transform);
            UI.transform.Find("PlayerNameText").GetComponent<Text>().text = info.Sender.NickName + " Killed " + info.photonView.Owner.NickName;
            Destroy(UI, 4.0f);
        }
    }

    private void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LoadLevel("LobbyScene");
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(HitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die(PhotonMessageInfo info)
    {
        if(photonView.IsMine)
        {
            // Event Stuff
            string nickname = photonView.Owner.NickName;
            object[] data = new object[] { nickname};
            RaiseEventOptions raiseEventOptions = new()
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
            };
            SendOptions sendOptions = new()
            {
                Reliability = false
            };
            PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoDiedCode, data, raiseEventOptions, sendOptions);

            GameObject[] go = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject g in go)
            {
                Debug.Log(g.GetComponent<PhotonView>().Owner.NickName);
                Debug.Log(info.photonView.Owner.NickName);
                //if (g.GetComponent<PhotonView>().Owner.NickName == info.photonView.Owner.NickName)
                //{
                //    g.GetComponent<BoxCollider>().enabled = false;
                //}
                if (g.GetComponent<PhotonView>().Owner.NickName == info.Sender.NickName)
                {
                    g.GetComponentInChildren<Camera>().enabled = true;
                }
            }
        }


        //GameObject[] go = GameObject.FindGameObjectsWithTag("Player");
        //foreach (GameObject g in go)
        //{
        //    if (g.GetComponent<PhotonView>().Owner.NickName == info.photonView.Owner.NickName)
        //    {
        //        g.GetComponent<BoxCollider>().enabled = false;
        //    }
        //    if (g.GetComponent<PhotonView>().Owner.NickName == info.Sender.NickName)
        //    {
        //        g.GetComponentInChildren<Camera>().enabled = true;
        //    }
        //}
    }
}
