using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    [SerializeField]
    public GameObject playerCamera;
    [SerializeField]
    private TextMeshProUGUI playerNameText;
    private void Start()
    {
        if(photonView.IsMine)
        {
            transform.GetComponent<MovementController>().enabled = true;
            GetComponent<FinishLineEval>().enabled = true;
            GetComponent<Shooting>().enabled = true;
            playerCamera.GetComponent<Camera>().enabled = true;
        }
        else
        {
            transform.GetComponent<MovementController>().enabled = false;
            GetComponent<FinishLineEval>().enabled = false;
            GetComponent<Shooting>().enabled = false;
            playerCamera.GetComponent<Camera>().enabled = false;
        }

        playerNameText.text = photonView.Owner.NickName;
    }
}
