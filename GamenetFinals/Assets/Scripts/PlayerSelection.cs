using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSelection : MonoBehaviour
{
    public GameObject[] SelectablePlayers;
    public int PlayerSelectionNumber;

    private void Start()
    {
        PlayerSelectionNumber = 0;

        ActivatePlayer(PlayerSelectionNumber);
    }

    private void ActivatePlayer(int x)
    {
        foreach(GameObject go in SelectablePlayers)
        {
            go.SetActive(false);
        }

        SelectablePlayers[x].SetActive(true);

        // Setting the player selection for his/her avatar
        ExitGames.Client.Photon.Hashtable playerSelectionProperties = new() { { Constants.PLAYER_SELECTION_NUMBER, PlayerSelectionNumber } };
        PhotonNetwork.LocalPlayer.SetCustomProperties(playerSelectionProperties);
    }

    public void GoToNextPlayer()
    {
        PlayerSelectionNumber++;
        if(PlayerSelectionNumber >= SelectablePlayers.Length)
        {
            PlayerSelectionNumber = 0;
        }
        ActivatePlayer(PlayerSelectionNumber);
    }

    public void GoToPreviousPlayer()
    {
        PlayerSelectionNumber--;
        if (PlayerSelectionNumber < 0)
        {
            PlayerSelectionNumber = SelectablePlayers.Length - 1;
        }
        ActivatePlayer(PlayerSelectionNumber);
    }
}
