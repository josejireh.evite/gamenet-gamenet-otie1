using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

public class CountdownManager : MonoBehaviourPunCallbacks
{
    public TextMeshProUGUI timerText;

    public float timeToStart = 5.0f;

    private void Start()
    {
        timerText = GameManager.instance.timeText;
    }

    private void Update()
    {
        if(PhotonNetwork.IsMasterClient)
        {
            if (timeToStart > 0)
            {
                timeToStart -= Time.deltaTime;
                photonView.RPC("SetTime", RpcTarget.AllBuffered, timeToStart);
            }
            else if (timeToStart < 0)
            {
                photonView.RPC("StartRace", RpcTarget.AllBuffered);
            }
        }
    }

    [PunRPC]
    public void SetTime(float time)
    {
        if(time >0)
        {
            timerText.text = time.ToString("F1");
        }
        else
        {
            timerText.text = "";
        }
    }

    [PunRPC]
    public void StartRace()
    {
        GetComponent<MovementController>().isControlEnabled = true;
        this.enabled = false;
    }
}
