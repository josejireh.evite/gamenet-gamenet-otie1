using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using TMPro;

public class FinishLineEval : MonoBehaviourPunCallbacks
{
    public enum RaiseEventsCode
    {
        WhoFinishedEventCode = 0
    }

    private int finishOrder = 0;

    // Adding Listeners
    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    // Callback Function for the listener
    void OnEvent(EventData photonEvent)
    {
        if(photonEvent.Code == (byte)RaiseEventsCode.WhoFinishedEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;

            string nicknameOfFinishedPlayer = (string)data[0];
            finishOrder = (int)data[1];
            int viewId = (int)data[2];

            Debug.Log(nicknameOfFinishedPlayer + " " + finishOrder);

            GameObject finishTextGameObject = GameManager.instance.finisherTextGameObject;
            finishTextGameObject.SetActive(true);
            if(viewId == photonView.ViewID)
            {
                finishTextGameObject.GetComponent<TextMeshProUGUI>().text = nicknameOfFinishedPlayer + " has Won the Game" + "(YOU)";
            }
            else
            {
                finishTextGameObject.GetComponent<TextMeshProUGUI>().text = nicknameOfFinishedPlayer + " has Won the Game";
            }
            StartCoroutine(FinishedGameCountdown());
        }
    }

    IEnumerator FinishedGameCountdown()
    {
        yield return new WaitForSeconds(2.5f);
        if (photonView.IsMine)
        {
            PhotonNetwork.Disconnect();
            PhotonNetwork.LoadLevel("LobbyScene");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Finish"))
        {
            Debug.Log("Done");
            GameFinish();
        }
    }

    private void GameFinish()
    {
        GetComponent<PlayerSetup>().playerCamera.transform.parent = null;
        GetComponent<MovementController>().enabled = false;

        finishOrder++;

        string nickname = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        // EventDATA
        object[] data = new object[] { nickname, finishOrder, viewId };

        // RaiseEventOptions
        RaiseEventOptions raiseEventOptions = new()
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        // Send Options
        SendOptions sendOptions = new()
        {
            Reliability = false
        };
        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoFinishedEventCode, data, raiseEventOptions, sendOptions);

    }
}
