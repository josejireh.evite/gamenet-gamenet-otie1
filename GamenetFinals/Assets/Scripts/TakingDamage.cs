using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class TakingDamage : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject whoKilledWhoPrefab;
    private GameObject whoKilledWhoParent;

    [SerializeField] MovementController movementController;

    [SerializeField] Image healthbar;

    [SerializeField] private float startHealth = 100;

    public float health;

    private void Start()
    {
        health = startHealth;
        healthbar.fillAmount = health / startHealth;
        whoKilledWhoParent = GameObject.Find("WhoKilledWhoParent");
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        health -= damage;
        Debug.Log(health);

        healthbar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            Die();

            //if(info.Sender != null)
            //{
            //    // Instantiing a prefab using the layout group thingy that can stack even if there is a lot if kills
            //    GameObject UI = Instantiate(whoKilledWhoPrefab, whoKilledWhoParent.transform);
            //    UI.gameObject.transform.SetParent(whoKilledWhoParent.transform);
            //    UI.transform.Find("PlayerNameText").GetComponent<Text>().text = info.Sender.NickName + " Killed " + info.photonView.Owner.NickName;
            //    Destroy(UI, 4.0f);
            //}

            // Instantiing a prefab using the layout group thingy that can stack even if there is a lot if kills
            GameObject UI = Instantiate(whoKilledWhoPrefab, whoKilledWhoParent.transform);
            UI.gameObject.transform.SetParent(whoKilledWhoParent.transform);
            UI.transform.Find("PlayerNameText").GetComponent<Text>().text = info.Sender.NickName + " Killed " + info.photonView.Owner.NickName;
            Destroy(UI, 4.0f);

            health = startHealth;
            healthbar.fillAmount = health / startHealth;
        }
    }

    private void Die()
    {
        if(photonView.IsMine)
        {
            movementController.ResetPosition();
        }
    }
}
