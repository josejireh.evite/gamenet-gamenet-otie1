using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    [SerializeField] float bulletSpeed = 150f; 
    private void Start()
    {
        Destroy(gameObject, 5.0f);
    }

    private void LateUpdate()
    {
        transform.Translate(Vector3.forward * 150f * Time.deltaTime);
    }
}
