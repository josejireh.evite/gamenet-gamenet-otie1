using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using Photon.Pun;
using TMPro;
public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera playerCamera;

    public GameObject HitEffectPrefab;

    [Header("HP Related Stuff")]
    public float startHealth = 100;
    private float health;
    public Image healthBar;

    private Animator animator;

    public GameObject WhoKilledWhoPrefabParent;
    public GameObject WhoKilledWhoPrefab;

    public int Kills;
    private void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        animator = GetComponent<Animator>();
        WhoKilledWhoPrefabParent = GameObject.Find("PlayerList");
    }
    public void Fire()
    {
        RaycastHit hit;
        Ray ray = playerCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if(Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);

            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            if(hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        health -= damage;
        healthBar.fillAmount = health / startHealth;

        if(health <= 0)
        {
            Die();

            Debug.Log(info.Sender.NickName + " Killed " + info.photonView.Owner.NickName);

            // Instantiing a prefab using the layout group thingy that can stack even if there is a lot if kills
            GameObject UI = Instantiate(WhoKilledWhoPrefab, WhoKilledWhoPrefabParent.transform);
            UI.gameObject.transform.SetParent(WhoKilledWhoPrefabParent.transform);
            UI.transform.Find("PlayerNameText").GetComponent<Text>().text = info.Sender.NickName + " Killed " + info.photonView.Owner.NickName;
            Destroy(UI, 4.0f);


            // Add Kill to the kiler
            GameManager.instance.playerInRoom[info.Sender.NickName] = GameManager.instance.playerInRoom[info.Sender.NickName] += 1;

            // Check if its more than 10
            if(GameManager.instance.playerInRoom[info.Sender.NickName] >= 10)
            {
                GameObject UIForWinner = Instantiate(WhoKilledWhoPrefab, WhoKilledWhoPrefabParent.transform);
                UIForWinner.gameObject.transform.SetParent(WhoKilledWhoPrefabParent.transform);
                UIForWinner.transform.Find("PlayerNameText").GetComponent<Text>().text = info.Sender.NickName + " won the game!";
                Invoke("LeaveRoom", 5.0f);
            }
        }
    }

    private void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LoadLevel("LobbyScene");
    }
    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(HitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die()
    {
        if(photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountdown());
        }
    }

    IEnumerator RespawnCountdown()
    {
        GameObject respawnText = GameObject.Find("RespawnText");
        if(respawnText == null)
        {
            Debug.Log("isNull");
        }
        float respawnTime = 5.0f;

        while(respawnTime > 0 )
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<TextMeshProUGUI>().text = "You are Killed, Respawning in " + respawnTime.ToString(".00");
        }

        animator.SetBool("isDead", false);
        respawnText.GetComponent<TextMeshProUGUI>().text = "";

        int randomPointX = Random.Range(-20, 20);
        int randomPointZ = Random.Range(-20, 20);

        transform.position = SpawnerManager.instance.RespawnSpots[Random.Range(0, SpawnerManager.instance.RespawnSpots.Length)].transform.position;

        transform.GetComponent<PlayerMovementController>().enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void RegainHealth()
    {
        health = 100;
        healthBar.fillAmount = health / startHealth;
    }
}
