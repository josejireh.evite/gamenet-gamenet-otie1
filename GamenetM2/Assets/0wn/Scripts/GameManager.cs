using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using Photon.Realtime;

public class GameManager : MonoBehaviourPunCallbacks
{
    public static GameManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    public GameObject playerPrefab;

    public Dictionary<string, int> playerInRoom;
    // Start is called before the first frame update
    void Start()
    {
        playerInRoom = new();

        if (PhotonNetwork.IsConnectedAndReady)
        {
            int randomPointX = Random.Range(-10, 10);
            int randomPointZ = Random.Range(-10, 10);
            PhotonNetwork.Instantiate(playerPrefab.name, 
                                    SpawnerManager.instance.RespawnSpots[Random.Range(0, SpawnerManager.instance.RespawnSpots.Length)].transform.position, 
                                    Quaternion.identity);
        }
        // Find all player to add them in the dictionary using their nickname which is prone to errors ngl
        Invoke("FindAllPlayers", 10.0f);
    }

    void FindAllPlayers()
    {
        GameObject[] player = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject playerPtr in player)
        {
            Debug.Log(playerPtr.GetComponent<PhotonView>().Owner.NickName);
            playerInRoom.Add(playerPtr.GetComponent<PhotonView>().Owner.NickName, 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.S))
        {
            foreach (KeyValuePair<string, int> entry in playerInRoom)
            {
                Debug.Log(entry.Key + " has " + entry.Value + " kills ");
            }
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        string playerName = newPlayer.NickName;
        playerInRoom.Add(playerName, 0);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        string playerName = otherPlayer.NickName;
        playerInRoom.Remove(playerName);
    }
}
